import datetime

import pandas as pd
from lifetimes.fitters.beta_geo_fitter import BetaGeoFitter


PREDICTION_WINDOW = 90
DECISION_THRESHOLD = 0.5
MIN_FREQUENCIES = {30: 0,
                   60: 0,
                   90: 0,
                   120: 1,
                   150: 1,
                   180: 1,
                   210: 2,
                   240: 2,
                   270: 2,
                   300: 3,
                   330: 3,
                   360: 3}


def get_churn_training_data(engine, max_date, prediction_point, prediction_window=PREDICTION_WINDOW):
    query = f"""

            with base as
            (select "Brand",
                    round("UserID") as "UserID",
                    "Date",
                    zeroifnull(sum("Real_money_bets")) as "RMBets",
                    zeroifnull(sum("Bonus_money_bets")) as "BMBets",
                    "RMBets" + "BMBets" as "TotalBets",
                    zeroifnull(sum("Real_money_win")) as "RMWin",
                    zeroifnull(sum("Bonus_money_win")) as "BMWin",
                    "RMWin" - "RMBets" as "NetRMWin",
                    "RMWin" + "BMWin" as "TotalWin",
                    "TotalWin" - "TotalBets" as "NetWin",
                    zeroifnull(sum("Deposit_Amount")) + zeroifnull(sum("Manual_Deposit_Amount")) as "TotalDeposits",
                    zeroifnull(sum("Withdrawal_Amount")) + zeroifnull(sum("Manual_Withdrawal_Amount")) as "TotalWithdrawals",
                    "TotalDeposits" - "TotalWithdrawals" as "NetDeposits",
                    -1 * zeroifnull(sum("BonusCashoutEUR")) as "BonusCashout",
                    zeroifnull(sum("JackpotPayout")) as "JP"
            from "EPICEROS"."ANALYTICS"."MGMT_REPORTING2"
            where "Brand" is not null
            and "Date" <= '{max_date}'
            group by 1, 2, 3),

            fbd as
            (select "Brand",
                    "UserID",
                    min("Date") as "FirstBetDate"
            from base
            where "TotalBets" > 0
            group by 1, 2),

            final as
            (select b."Brand",
                    b."UserID",
                    b."Date",
                    datediff('day', f."FirstBetDate", b."Date") as "LifecycleDay",
                    b."RMBets",
                    b."BMBets",
                    b."TotalBets",
                    b."RMWin",
                    b."BMWin",
                    b."NetRMWin",
                    b."TotalWin",
                    b."NetWin",
                    b."TotalDeposits",
                    b."TotalWithdrawals",
                    b."NetDeposits",
                    b."BonusCashout",
                    b."JP"
            from base b
            left join fbd f
            on b."Brand" = f."Brand"
            and b."UserID" = f."UserID"
            where "LifecycleDay" <= {prediction_point + prediction_window + 1}),

            train_period_aggs as
            (select f."Brand",
                    f."UserID",
                    sum(f."RMBets") as "RMBets",
                    sum(f."BMBets") as "BMBets",
                    sum(f."TotalBets") as "TotalBetsAgg",
                    sum(f."RMWin") as "RMWin",
                    sum(f."BMWin") as "BMWin",
                    sum(f."NetRMWin") as "NetRMWin",
                    sum(f."TotalWin") as "TotalWinAgg",
                    sum(f."NetWin") as "NetWin",
                    case when "TotalBetsAgg" > 0 then "TotalWinAgg" / "TotalBetsAgg" else 1 end as "RTP",
                    sum(f."TotalDeposits") as "TotalDepositsAgg",
                    sum(f."TotalWithdrawals") as "TotalWithdrawalsAgg",
                    sum(f."NetDeposits") as "NetDeposits",
                    case when "TotalDepositsAgg" > 0 then "TotalWithdrawalsAgg" / "TotalDepositsAgg" else 1 end as "WithdrawalRatio",
                    sum(f."BonusCashout") as "BonusCashout",
                    sum(f."JP") as "JP"
            from final f
            where "LifecycleDay" <= {prediction_point}
            group by 1, 2),

            rfm as
            (select f."Brand", 
                    f."UserID",
                    count(distinct case when f."LifecycleDay" <= {prediction_point} and f."TotalBets" > 0 then "Date" end) - 1 as "frequency", 
                    max(case when f."LifecycleDay" <= {prediction_point} and f."TotalBets" > 0 then "LifecycleDay" end) as "recency",
                    {prediction_point} as "T",
                    sum(case when f."LifecycleDay" between {prediction_point + 1} and {prediction_point + prediction_window + 1} and f."TotalBets" > 0 then "LifecycleDay" else 0 end) = 0 as "churn_test_period"
            from final f
            group by 1, 2)

            select r."Brand",
                   r."UserID",
                   r."frequency", 
                   r."recency",
                   r."T",
                   r."churn_test_period",
                   tra."RMBets",
                   tra."BMBets",
                   tra."TotalBetsAgg",
                   tra."RMWin",
                   tra."BMWin",
                   tra."NetRMWin",
                   tra."TotalWinAgg",
                   tra."NetWin",
                   tra."RTP",
                   tra."TotalDepositsAgg",
                   tra."TotalWithdrawalsAgg",
                   tra."NetDeposits",
                   tra."WithdrawalRatio",
                   tra."BonusCashout",
                   tra."JP"
            from rfm r
            left join train_period_aggs tra
            on r."Brand" = tra."Brand"
            and r."UserID" = tra."UserID"
            where r."recency" >= greatest(0, {prediction_point - prediction_window});

    """

    df = pd.read_sql(query, con=engine)

    return df


def get_churn_prediction_data(engine, max_date):
    query = f"""

            with base as
            (select "Brand",
                    round("UserID") as "UserID",
                    "Date",
                    zeroifnull(sum("Real_money_bets")) as "RMBets",
                    zeroifnull(sum("Bonus_money_bets")) as "BMBets",
                    "RMBets" + "BMBets" as "TotalBets",
                    zeroifnull(sum("Real_money_win")) as "RMWin",
                    zeroifnull(sum("Bonus_money_win")) as "BMWin",
                    "RMWin" - "RMBets" as "NetRMWin",
                    "RMWin" + "BMWin" as "TotalWin",
                    "TotalWin" - "TotalBets" as "NetWin",
                    zeroifnull(sum("Deposit_Amount")) + zeroifnull(sum("Manual_Deposit_Amount")) as "TotalDeposits",
                    zeroifnull(sum("Withdrawal_Amount")) + zeroifnull(sum("Manual_Withdrawal_Amount")) as "TotalWithdrawals",
                    "TotalDeposits" - "TotalWithdrawals" as "NetDeposits",
                    -1 * zeroifnull(sum("BonusCashoutEUR")) as "BonusCashout",
                    zeroifnull(sum("JackpotPayout")) as "JP"
            from "EPICEROS"."ANALYTICS"."MGMT_REPORTING2"
            where "Brand" is not null
            and "Date" <= '{max_date}'
            group by 1, 2, 3),

            fbd as
            (select "Brand",
                    "UserID",
                    min("Date") as "FirstBetDate"
            from base
            where "TotalBets" > 0
            group by 1, 2),

            final as
            (select b."Brand",
                    b."UserID",
                    b."Date",
                    datediff('day', f."FirstBetDate", b."Date") as "LifecycleDay",
                    b."RMBets",
                    b."BMBets",
                    b."TotalBets",
                    b."RMWin",
                    b."BMWin",
                    b."NetRMWin",
                    b."TotalWin",
                    b."NetWin",
                    b."TotalDeposits",
                    b."TotalWithdrawals",
                    b."NetDeposits",
                    b."BonusCashout",
                    b."JP"
            from base b
            left join fbd f
            on b."Brand" = f."Brand"
            and b."UserID" = f."UserID"),

            train_period_aggs as
            (select f."Brand",
                    f."UserID",
                    sum(f."RMBets") as "RMBets",
                    sum(f."BMBets") as "BMBets",
                    sum(f."TotalBets") as "TotalBetsAgg",
                    sum(f."RMWin") as "RMWin",
                    sum(f."BMWin") as "BMWin",
                    sum(f."NetRMWin") as "NetRMWin",
                    sum(f."TotalWin") as "TotalWinAgg",
                    sum(f."NetWin") as "NetWin",
                    case when "TotalBetsAgg" > 0 then "TotalWinAgg" / "TotalBetsAgg" else 1 end as "RTP",
                    sum(f."TotalDeposits") as "TotalDepositsAgg",
                    sum(f."TotalWithdrawals") as "TotalWithdrawalsAgg",
                    sum(f."NetDeposits") as "NetDeposits",
                    case when "TotalDepositsAgg" > 0 then "TotalWithdrawalsAgg" / "TotalDepositsAgg" else 1 end as "WithdrawalRatio",
                    sum(f."BonusCashout") as "BonusCashout",
                    sum(f."JP") as "JP"
            from final f
            group by 1, 2),

            rfm as
            (select f."Brand", 
                    f."UserID",
                    count(distinct case when f."TotalBets" > 0 then "Date" end) - 1 as "frequency", 
                    max(case when f."TotalBets" > 0 then "LifecycleDay" end) as "recency",
                    max("LifecycleDay") as "T"
            from final f
            group by 1, 2)

            select r."Brand",
                   r."UserID",
                   r."frequency", 
                   r."recency",
                   r."T",
                   tra."RMBets",
                   tra."BMBets",
                   tra."TotalBetsAgg",
                   tra."RMWin",
                   tra."BMWin",
                   tra."NetRMWin",
                   tra."TotalWinAgg",
                   tra."NetWin",
                   tra."RTP",
                   tra."TotalDepositsAgg",
                   tra."TotalWithdrawalsAgg",
                   tra."NetDeposits",
                   tra."WithdrawalRatio",
                   tra."BonusCashout",
                   tra."JP"
            from rfm r
            left join train_period_aggs tra
            on r."Brand" = tra."Brand"
            and r."UserID" = tra."UserID";

    """

    df = pd.read_sql(query, con=engine)

    return df


def build_rfm_features_training(df,
                                bgf,
                                prediction_point,
                                prediction_window=PREDICTION_WINDOW,
                                min_frequencies=None,
                                decision_threshold=DECISION_THRESHOLD):
    if not min_frequencies:
        min_frequencies = MIN_FREQUENCIES

    df['probability_alive'] = bgf.conditional_probability_alive(df['frequency'], df['recency'], df['t'])
    df['expected_active_days'] = bgf.conditional_expected_number_of_purchases_up_to_time(prediction_window,
                                                                                         df['frequency'],
                                                                                         df['recency'],
                                                                                         df['t'])
    df['expected_active_days'].fillna(0, inplace=True)
    df['churn_predicted'] = df['probability_alive'] <= decision_threshold
    df.loc[df['frequency'] <= min_frequencies[prediction_point], 'churn_predicted'] = True

    return df


def build_rfm_features_prediction(group,
                                  churn_models,
                                  prediction_window=PREDICTION_WINDOW,
                                  min_frequencies=None,
                                  decision_threshold=DECISION_THRESHOLD):
    if not min_frequencies:
        min_frequencies = MIN_FREQUENCIES

    prediction_point = group.name
    bgf = BetaGeoFitter()
    bgf.params_ = churn_models.loc[churn_models['PredictionPoint'] == prediction_point, 'BGF'].values[0]
    group['probability_alive'] = bgf.conditional_probability_alive(group['frequency'],
                                                                   group['recency'],
                                                                   group['t'])
    group['expected_active_days'] = bgf.conditional_expected_number_of_purchases_up_to_time(prediction_window,
                                                                                            group['frequency'],
                                                                                            group['recency'],
                                                                                            group['t'])
    group['expected_active_days'].fillna(0, inplace=True)
    group['churn_predicted'] = group['probability_alive'] <= decision_threshold
    group.loc[group['frequency'] <= min_frequencies[prediction_point], 'churn_predicted'] = True

    return group
