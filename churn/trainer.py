import datetime

import pandas as pd
from lifetimes.fitters.beta_geo_fitter import BetaGeoFitter
from sklearn.ensemble import RandomForestClassifier
from sklearn.model_selection import train_test_split
from sklearn.metrics import accuracy_score, precision_score, recall_score

from utils import save_model
from churn.data_prepper import get_churn_training_data, build_rfm_features_training


DIR_PATH = 'churn/models'
FILE_NAME = 'churn_models.joblib'
PREDICTION_WINDOW = 90
PREDICTION_POINTS = [30, 60, 90, 120, 150, 180, 210, 240, 270, 300, 330, 360]
MAX_DATE = datetime.date.today().strftime('%Y-%m-%d')


def train_bgf(df):
    try:
        bgf = BetaGeoFitter(penalizer_coef=0.5)
        bgf.fit(df['frequency'], df['recency'], df['t'])
    except Exception as e:
        bgf = BetaGeoFitter(penalizer_coef=1)
        bgf.fit(df['frequency'], df['recency'], df['t'])

    return bgf


def train_churn_forest(df):
    df.set_index(['Brand', 'UserID'], inplace=True)
    X = df.drop('churn_test_period', axis=1)
    y = df['churn_test_period']

    X_train, X_test, y_train, y_test = train_test_split(X,
                                                        y,
                                                        test_size=0.33,
                                                        random_state=0)

    clf = RandomForestClassifier(random_state=0)
    clf.fit(X_train, y_train)

    y_pred = clf.predict(X_test)
    a = accuracy_score(y_test, y_pred)
    p = precision_score(y_test, y_pred)
    r = recall_score(y_test, y_pred)
    true_churn_percent = y_test.mean()

    return {'Model': clf,
            'Accuracy': a,
            'Precision': p,
            'Recall': r,
            'TrueChurnPercent': true_churn_percent}


def train_and_save_churn_models(engine, logger, max_date, prediction_points=None):
    try:
        if not max_date:
            max_date = MAX_DATE

        logger.info(f'Running churn trainer - max date: {max_date}')

        if not prediction_points:
            prediction_points = PREDICTION_POINTS

        # rows = []

        for pp in prediction_points:
            logger.info(f'Training churn model at prediction point {pp}')

            df = get_churn_training_data(engine=engine, max_date=max_date, prediction_point=pp)
            logger.info('Churn model data retrieved')

            bgf = train_bgf(df)
            logger.info('BGF trained successfully')

            df = build_rfm_features_training(df, bgf, pp)
            logger.info('RFM features built successfully')

            res = train_churn_forest(df)
            logger.info('Random forest built successfully')
            res['PredictionPoint'] = pp
            res['BGF'] = bgf.params_
            # rows.append(res)

            # saving files iteratively and retrying once to handle inconsistent pickling behavior - TODO fix
            try:
                churn_models = pd.DataFrame([res])
                save_model(model_obj=churn_models, dir_path=DIR_PATH, file_name=f'{pp}.joblib')
                logger.info(f'Churn model for prediction point {pp} saved')
            except Exception as e:
                logger.info(e)
                try:
                    churn_models = pd.DataFrame([res])
                    save_model(model_obj=churn_models, dir_path=DIR_PATH, file_name=f'{pp}.joblib')
                    logger.info(f'Churn model for prediction point {pp} saved')
                except Exception as e:
                    logger.info(e)
                    continue
        # churn_models = pd.DataFrame(rows)
        # save_model(model_obj=churn_models, dir_path=DIR_PATH, file_name=FILE_NAME)
        # logger.info('Churn models saved')

        return True
    except Exception as e:
        logger.info(f'{e} while training churn models')
        return False
