import datetime

import pandas as pd

from utils import load_model
from churn.data_prepper import get_churn_prediction_data, build_rfm_features_prediction


DIR_PATH = 'churn/models'
FILE_NAME = 'churn_models.joblib'
MIN_FREQUENCIES = {30: 0,
                   60: 0,
                   90: 0,
                   120: 1,
                   150: 1,
                   180: 1,
                   210: 2,
                   240: 2,
                   270: 2,
                   300: 3,
                   330: 3,
                   360: 3}
PREDICTION_POINTS = [30, 60, 90, 120, 150, 180, 210, 240, 270, 300, 330, 360]
DECISION_THRESHOLD = 0.5
PREDICTION_WINDOW = 90
MAX_DATE = datetime.date.today().strftime('%Y-%m-%d')


def choose_prediction_point(t):
    if t >= 360:
        prediction_point = 360
    elif t > 0 and t % 30 == 0:
        prediction_point = t
    else:
        prediction_point = ((t // 30) + 1) * 30

    return prediction_point


def churn_forest_predict(group, churn_models):
    prediction_point = group.name
    clf = churn_models.loc[churn_models['PredictionPoint'] == prediction_point, 'Model'].values[0]

    group.set_index(['Brand', 'UserID'], inplace=True)
    group.drop('PredictionPoint', axis=1, inplace=True)

    churn_predicted = clf.predict(group)
    group['ChurnProbability'] = clf.predict_proba(group)[:, 1]  # depends on ordering of classes: prob for t and f returned
    group['ChurnPredicted'] = churn_predicted  # can't add directly as it ruins prob prediction

    return group


def make_and_save_churn_predictions(engine, logger, max_date):
    try:
        if not max_date:
            max_date = MAX_DATE

        logger.info(f'Running churn predictor - max date: {max_date}')

        churn_models = pd.concat([load_model(dir_path=DIR_PATH, file_name=f'{pp}.joblib')
                                  for pp in PREDICTION_POINTS])
        logger.info('Churn models loaded')

        df = get_churn_prediction_data(engine=engine, max_date=max_date)
        logger.info('Churn prediction data retrieved')

        df['PredictionPoint'] = df['t'].apply(choose_prediction_point)
        logger.info('Churn prediction points chosen')

        df = df.groupby('PredictionPoint').apply(build_rfm_features_prediction, churn_models=churn_models)
        logger.info('Churn RFM features built')

        df = df.groupby('PredictionPoint').apply(churn_forest_predict, churn_models=churn_models)
        logger.info('Churn predictions made successfully')

        df = df[['ChurnPredicted', 'ChurnProbability']]
        df['PredictionTimestamp'] = max_date

        df.to_sql(name='CHURN_PREDICTIONS_HISTORY',
                  con=engine,
                  if_exists='append',
                  index=True,
                  chunksize=10000)
        logger.info('Churn model predictions saved to DB')

        return True
    except Exception as e:
        logger.info(f'{e} while making churn predictions')
        return False
