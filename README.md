# Deployment Notes
 - App is run with `api.py` from parent directory
 - Need python version 3.9.6
 - May need some sort of JDBC driver to handle DB connection
 - Need access to Epiceros (EU) Prod Snowflake read (select) and write (insert)
 - Need file `.env` in parent directory with `SNOWFLAKE_USER=XXX` and `SNOWFLAKE_PASS=XXX` credentials on separate lines
 - Need ~3gb hard drive space (2gb but extra to be safe)
 - Approximate time/RAM for different operations:
    - Churn training (once/month): 45 minutes, 2.5 gb ram
    - Hold training (once/month): 1 minute, 3gb ram
    - Churn prediction (once/day): 10 minutes, 7.5gb ram
    - Hold prediction (once/day): 1 minutes, 1gb ram

# TODO
 - try to swap local saves for s3
 - adjust logic for high win numbers: switch features to "relative win number (-1, -2, etc)" and predict on recent wins, not up to 50th