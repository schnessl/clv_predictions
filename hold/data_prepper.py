import pandas as pd


MAX_WIN_NUMBER = 50


def get_hold_data(engine, max_date, max_win_number=MAX_WIN_NUMBER):
    query = f"""

    with latest_eligibility_data as
    (select "Brand",
            "UserID",
            "Eligible"
    from "EPICEROS"."CLV"."PLAYER_ELIGIBILITY_HISTORY"
    where "EligibilityTimestamp"::date <= greatest('{max_date}', (select max("EligibilityTimestamp")
                                                                  from "EPICEROS"."CLV"."PLAYER_ELIGIBILITY_HISTORY"))
    qualify row_number() over (partition by "Brand", "UserID" order by "EligibilityTimestamp" desc) = 1),

    base as
    (select mr."Brand",
            round(mr."UserID") as "UserID",
            mr."Date",
            zeroifnull(sum(mr."Real_money_bets")) as "RMBets",
            zeroifnull(sum(mr."Real_money_win")) as "RMWin",
            "RMWin" - "RMBets" as "NetRMWin",
            zeroifnull(sum(mr."Withdrawal_Amount")) + zeroifnull(sum("Manual_Withdrawal_Amount")) as "TotalWithdrawals",
            -1 * zeroifnull(sum(mr."BonusCashoutEUR")) as "BonusCashout",
            zeroifnull(sum(mr."JackpotPayout")) as "JP",
            "BonusCashout" + "JP" as "Freebies",
            "NetRMWin" + "Freebies" as "DailyGain",
            "TotalWithdrawals" > 0 as "AnyWithdrawal"
    from "EPICEROS"."ANALYTICS"."MGMT_REPORTING2" mr
    where "Brand" is not null
    and "Date" <= '{max_date}'
    and "UserID" in (select distinct "UserID"
                     from latest_eligibility_data
                     where "Eligible")
    group by 1, 2, 3),

    wins as
    (select "Brand",
            "UserID",
            "Date",
            row_number() over (partition by "Brand", "UserID" order by "Date") as "WinNumber"
    from base
    where "DailyGain" > 0)

    select w."Brand",
           w."UserID",
           w."WinNumber",
           sum(b."TotalWithdrawals") = 0 as "100%Hold"
    from wins w
    left join base b
    on w."Brand" = b."Brand"
    and w."UserID" = b."UserID"
    and b."Date" >= w."Date"
    and b."Date" < dateadd('day', 3, w."Date")
    where w."WinNumber" <= {max_win_number}
    group by 1, 2, 3
    order by 1, 2, 3;

    """

    raw_df = pd.read_sql(query, con=engine)

    return raw_df


def prep_hold_feature_df(max_date,
                         engine=None,
                         raw_df=None,
                         max_win_number=MAX_WIN_NUMBER):
    if raw_df is None and not engine:
        raise ('Must pass either a DB connection or df in appropriate format')

    if raw_df is None:
        raw_df = get_hold_data(engine=engine,
                               max_date=max_date,
                               max_win_number=max_win_number)

    feature_df = raw_df.pivot(index=['Brand', 'UserID'], columns='WinNumber', values='100%Hold')
    feature_df = feature_df.reset_index().rename_axis(None, axis=1)
    feature_df = feature_df.set_index(['Brand', 'UserID'])

    return feature_df


def get_deposit_data(engine, max_date):
    query = f"""

    select "Brand",
           "UserID",
           "PredictedDeposits"
    from "EPICEROS"."CLV"."DEPOSIT_PREDICTIONS_HISTORY"
    where "PredictionTimestamp"::date <= greatest('{max_date}', (select max("EligibilityTimestamp")
                                                                 from "EPICEROS"."CLV"."PLAYER_ELIGIBILITY_HISTORY"))
    qualify row_number() over (partition by "Brand", "UserID" order by "PredictionTimestamp" desc) = 1;

    """

    deposit_df = pd.read_sql(query, con=engine)

    return deposit_df


def prep_hold_prediction_df(raw_df, deposit_df):
    max_wins = raw_df.groupby('UserID')['WinNumber'].max().reset_index()

    prediction_df = deposit_df.merge(max_wins, how='left', on='UserID')
    prediction_df = prediction_df.set_index(['Brand', 'UserID'])

    return prediction_df
