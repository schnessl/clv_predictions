import datetime

import pandas as pd
from sklearn.ensemble import RandomForestClassifier
from sklearn.metrics import accuracy_score

from utils import save_model
from hold.data_prepper import prep_hold_feature_df


DIR_PATH = 'hold/models'
FILE_NAME = 'hold_models.joblib'
MIN_WIN_NUMBER = 2
MAX_WIN_NUMBER = 50
MAX_DATE = datetime.date.today().strftime('%Y-%m-%d')


def prep_data_for_win_number(feature_df, win_number):
    temp = feature_df[feature_df[win_number].notnull()][[col
                                                         for col in range(1, win_number + 1)]].copy().astype(int)

    test_users = pd.Series(feature_df.index.values).sample(frac=0.2, random_state=0)

    train = temp[~temp.index.isin(test_users)].copy()
    test = temp[temp.index.isin(test_users)].copy()

    y_train = train[win_number]
    X_train = train.drop(win_number, axis=1)

    y_test = test[win_number]
    X_test = test.drop(win_number, axis=1)

    return X_train, y_train, X_test, y_test


def train_model_and_report_scores(X_train, y_train, X_test, y_test):
    rf = RandomForestClassifier(random_state=0)
    rf.fit(X_train, y_train)

    y_pred = rf.predict(X_test)

    a = accuracy_score(y_test, y_pred, normalize=True)

    return rf, a


def train_and_save_hold_models(engine,
                               logger,
                               max_date,
                               min_win_number=MIN_WIN_NUMBER,
                               max_win_number=MAX_WIN_NUMBER):
    try:
        if not max_date:
            max_date = MAX_DATE

        logger.info(f'Running hold trainer - max date: {max_date}')

        feature_df = prep_hold_feature_df(max_date=max_date, engine=engine)
        logger.info('Hold training data retrieved')

        rows = []

        for win_number in range(min_win_number, max_win_number + 1):
            X_train, y_train, X_test, y_test = prep_data_for_win_number(feature_df, win_number)
            rf, a = train_model_and_report_scores(X_train, y_train, X_test, y_test)
            logger.info(f'Win number {win_number} trained')

            results = {'WinNumber': win_number, 'Model': rf,
                       'Accuracy': a, 'TrainingTimestamp': max_date}
            rows.append(results)

        logger.info('Hold model training complete')

        hold_models = pd.DataFrame(rows)
        save_model(model_obj=hold_models, dir_path=DIR_PATH, file_name=FILE_NAME)
        logger.info('Hold models saved')

        return True
    except Exception as e:
        logger.info(f'{e} while training hold models')
        return False

