import datetime

from utils import load_model
from hold.data_prepper import get_hold_data, get_deposit_data, prep_hold_feature_df, prep_hold_prediction_df


DIR_PATH = 'hold/models'
FILE_NAME = 'hold_models.joblib'
MAX_WIN_NUMBER = 50
KNOWN_AGG_HOLD = 0.47
MAX_DATE = datetime.date.today().strftime('%Y-%m-%d')


def give_burner_score(group, feature_df, hold_models, max_win_number=MAX_WIN_NUMBER):
    win_number = group.name + 1

    if win_number > max_win_number:
        win_number = max_win_number

    model = hold_models.loc[hold_models['WinNumber'] == win_number, 'Model'].values[0]
    accuracy = hold_models.loc[hold_models['WinNumber'] == win_number, 'Accuracy'].values[0]
    X = feature_df[feature_df.index.isin(group.index)][[col
                                                        for col in feature_df.columns
                                                        if col < win_number]].copy().astype(int)

    pred = model.predict(X)[0]

    if pred:
        score = accuracy
    else:
        score = 1 - accuracy

    try:
        group['BurnerScore'] = score
    except Exception as e:
        group['BurnerScore'] = None

    return group


def adjust_burner_scores(prediction_df, known_agg_hold=KNOWN_AGG_HOLD):
    total_dep = prediction_df['PredictedDeposits'].sum()
    prediction_df['SimpleExpectedWithdrawals'] = (1 - prediction_df['BurnerScore']) * prediction_df['PredictedDeposits']

    total_withdrawals_expected_based_on_hold = total_dep * known_agg_hold
    total_withdrawals_predicted_simple = prediction_df['SimpleExpectedWithdrawals'].sum()

    withdrawal_expectation_dif_abs = total_withdrawals_predicted_simple - total_withdrawals_expected_based_on_hold
    withdrawal_expectation_dif_rel = withdrawal_expectation_dif_abs / total_withdrawals_predicted_simple

    prediction_df['AdjustedBurnerScore'] = (prediction_df['BurnerScore'] + withdrawal_expectation_dif_rel).apply(
        lambda x: max(x, 0)).apply(lambda x: min(x, 1))
    prediction_df['AdjustedExpectedWithdrawals'] = (1 - prediction_df['AdjustedBurnerScore']) * prediction_df[
        'PredictedDeposits']

    return prediction_df


def make_and_save_hold_predictions(engine, logger, max_date):
    try:
        if not max_date:
            max_date = MAX_DATE

        logger.info(f'Running hold predictor - max date: {max_date}')

        hold_models = load_model(dir_path=DIR_PATH, file_name=FILE_NAME)
        logger.info('Hold models loaded')

        raw_df = get_hold_data(engine=engine, max_date=max_date)
        deposit_df = get_deposit_data(engine=engine, max_date=max_date)
        prediction_df = prep_hold_prediction_df(raw_df=raw_df, deposit_df=deposit_df)
        feature_df = prep_hold_feature_df(max_date=max_date, raw_df=raw_df)
        logger.info('Hold prediction data retrieved')

        prediction_df = prediction_df.groupby('WinNumber').apply(give_burner_score,
                                                                 feature_df=feature_df,
                                                                 hold_models=hold_models)

        prediction_df = adjust_burner_scores(prediction_df=prediction_df)
        logger.info('Hold predictions complete')

        prediction_df['PredictionTimestamp'] = max_date

        prediction_df.to_sql(name='HOLD_PREDICTIONS_HISTORY',
                             con=engine,
                             if_exists='append',
                             index=True,
                             chunksize=10000)
        logger.info('Hold model predictions saved to DB')
        return True
    except Exception as e:
        logger.info(f'{e} while making hold predictions')
        return False
