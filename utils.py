import os

import joblib
from dotenv import dotenv_values
from sqlalchemy import create_engine


def create_engine_snowflake(db='EPICEROS', schema='CLV', instance='epiceros.eu-central-1',
                            warehouse='MATILLION_ETL', username=None, password=None):
    if not username:
        username = dotenv_values('.env')['SNOWFLAKE_USER']

    if not password:
        password = dotenv_values('.env')['SNOWFLAKE_PASS']

    uri = 'snowflake://{username}:{password}@{instance}/{db}/{schema}?warehouse={warehouse}&role=SYSADMIN'.format(
        username=username,
        password=password,
        instance=instance,
        db=db,
        schema=schema,
        warehouse=warehouse)

    engine = create_engine(uri)

    return engine


def save_model(model_obj, dir_path, file_name):
    try:
        with open(os.path.join(dir_path, file_name), 'wb') as f:
            joblib.dump(model_obj, f, compress=3)
    except FileNotFoundError:
        os.mkdir(dir_path)
        with open(os.path.join(dir_path, file_name), 'wb') as f:
            joblib.dump(model_obj, f, compress=3)


def load_model(dir_path, file_name):
    try:
        with open(os.path.join(dir_path, file_name), 'rb') as f:
            models = joblib.load(f)

        return models
    except FileNotFoundError:
        raise ('Did not find trained model file.')