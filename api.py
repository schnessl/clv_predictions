import flask
from flask import request

from utils import create_engine_snowflake
from hold.trainer import train_and_save_hold_models
from hold.predictor import make_and_save_hold_predictions
from churn.trainer import train_and_save_churn_models
from churn.predictor import make_and_save_churn_predictions


app = flask.Flask(__name__)
app.config['DEBUG'] = True

engine = create_engine_snowflake()


@app.route('/')
def index():
    return """
    <h1>Available endpoints:</h1>
    <ul>
        <li>/train_hold</li>
        <li>/predict_hold</li>
        <li>/train_churn</li>
        <li>/predict_churn</li>
    </ul>
    """


@app.route('/train_hold', methods=['GET'])
def train_hold():
    max_date = request.args.get('max_date')
    success = train_and_save_hold_models(engine=engine, logger=app.logger, max_date=max_date)
    if success:
        return """<h1>Hold model training successful</h1>"""
    else:
        return """<h1>Hold model training unsuccessful</h1>"""


@app.route('/predict_hold', methods=['GET'])
def predict_hold():
    max_date = request.args.get('max_date')
    success = make_and_save_hold_predictions(engine=engine, logger=app.logger, max_date=max_date)
    if success:
        return """<h1>Hold model prediction successful</h1>"""
    else:
        return """<h1>Hold model prediction unsuccessful</h1>"""


@app.route('/train_churn', methods=['GET'])
def train_churn():
    max_date = request.args.get('max_date')
    success = train_and_save_churn_models(engine=engine, logger=app.logger, max_date=max_date)
    if success:
        return """<h1>Churn model training successful</h1>"""
    else:
        return """<h1>Churn model training unsuccessful</h1>"""


@app.route('/predict_churn', methods=['GET'])
def predict_churn():
    max_date = request.args.get('max_date')
    success = make_and_save_churn_predictions(engine=engine, logger=app.logger, max_date=max_date)
    if success:
        return """<h1>Churn model prediction successful</h1>"""
    else:
        return """<h1>Churn model prediction unsuccessful</h1>"""


if __name__ == '__main__':
    app.run()
